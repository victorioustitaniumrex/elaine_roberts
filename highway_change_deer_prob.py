from car import *
import random
import matplotlib.pyplot as plott
import matplotlib.animation as anim
import time as TIME

deer_prob_lst = []
trials_avg = []
deer_prob = -1.0
for v in range(50):
    deer_prob += 2.0
    trials = []

    for z in range(10):
        cars = []
        num_cars = 0
        time = 0.0
        dt=.1
        highwaylength=999
        delay=0
        firstRun=1

       





        printGraph=0 #Change to 1 to run

        if printGraph:
            fig=plott.figure()
            ax1=fig.add_subplot(1,1,1)
            plott.ion()

            plott.show()

        while len(cars)>0 or firstRun:
            time += 1
            #The five second delay functionality
            if not firstRun:
               if cars[-1].get_vel()<1 and cars[-1].get_pos()<=1:
                    delay=5
            firstRun=0
            if delay<=0:
                if num_cars <= highwaylength:
                    try:
        		      cars.append(car(cars[-1]))
                    except IndexError:
        		      cars.append(car())
                    num_cars += 1
            else:
                delay-=1

            cars_to_remove=[]
            for k in range(100):
                if 100.0 * random.random() <= deer_prob:
                    for m in range(len(cars)):
                        if k - cars[m].get_pos() <= 1.0 and k - cars[m].get_pos() > 0.0:
                            #cars[m].slowdown(0.5)
                            for i in range(10):
                                try:
                                    cars[m+i].slowdown(0.5)
                                except:
                                    pass
                            break

            for n in range(10):
                num_del = 0
                for i in range(len(cars)):
                    if cars[i-num_del].get_pos() >= 100.0:    #With just >, there'd be a max of 101 cars on the highway
                        del cars[i-num_del]
        		num_del += 1
                    else:               
                        cars[i-num_del].update(dt)

            if len(cars) == 0:
                #print "broke"
                break

            if printGraph:
                plott.clf()
        	plott.axis([0,100,-1,1])
                plott.plot([b.get_pos() for b in cars],[0]*len(cars),"b.")
                TIME.sleep(.01)

                plott.draw()


        #print cars
        #print num_cars
        #print time
        #print deer_prob
        trials.append(time)
    trials_avg.append(sum(trials)/float(len(trials)))
    deer_prob_lst.append(deer_prob)
print trials_avg
print deer_prob_lst
plott.xlabel('Deer Chance (Number of times out of one hundred)')
plott.ylabel('Time (sec)')
plott.title('Time for the 1000th Car to Leave the Highway as a Function of Deer Chance')
plott.plot(deer_prob_lst, trials_avg, '.r-')
plott.show()