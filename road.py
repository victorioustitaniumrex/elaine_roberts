from multiprocessing import Pool
import time
import weakref
import numpy as np
from car import *
import matplotlib.pyplot as plt
import random

class highway:
	def __init__(self,length=100):
		"""
		initialize a highway with a specified length
		"""
		self.length=length
		self.cars=[]
		self.num_cars=0
		self.initial=True
		self.dt=0.01
		self.curtime=0.0
		self.waiting=False
		self.start_wait=0.0
		self.tot_cars=0
		self.slowdown = 0.5
		self.deer = [0]*self.length

	def set_deer(self, num_deer=1, probability=0.5):
		"""
		set if a deer jumps out at a given mile marker.
		num_deer specifies how many deer jump out at each call
		set to 0 to make a random assignment

		if num_deer == 0, probability tells us the probability
		a deer will jump out at each spot
		"""
		if num_deer<=0:
			options=[0,1]
			for i in range(self.length):
				self.deer[i] = 1 if random.random()<probability else 0
			return
		if num_deer>self.length:
			num_deer=self.length
		options = range(self.length)
		self.deer = [0]*self.length
		for i in range(num_deer):
			indx = random.choice(options)
			options = [x-1 if x>indx else x for x in options]
			del options[indx]
			self.deer[indx]=1

	def get_positions(self):
		"""
		get positions of all cars on road
		"""
		return [x.get_pos() for x in self.cars]

	def get_velocities(self):
		"""
		get velocities for all cars on road
		"""
		return [x.get_vel() for x in self.cars]

	def get_deer(self):
		"""
		return a list of our deer locations
		"""
		return [i+1 for i,x in enumerate(self.deer) if x]
	
	def slow_for_deer(self,num_cars=1):
		"""
		slow the nearest car for all the deer. If the car more than a mile away, it won't slow.
		"""
		positions = self.get_positions()
		for i,val in enumerate(self.deer):
			if val:
				pos = self.find_closest_car(i+1)
				if pos>=0:
					for j in range(pos,min(len(self.cars),pos+num_cars)):
						self.cars[j].slowdown(self.slowdown)

	def find_closest_car(self,location):
		"""
		find the closest car to a location (location must be in front of car)
		returns car index (or -1 if no car)
		"""
		positions = self.get_positions()
		distance = [i for i,x in enumerate(positions) if location-x>0.0 and location-x<1.0]
		if len(distance)>0:
			return min(distance)
		return -1
	
	def run(self,dt=0.01,cars=1000,slowdown=0.5,N=10,deer_timer=1,deer_count=0,deer_prob=0.5,print_highway=False):
		"""
		run a simulation and print out how many timesteps it took

		dt - timestep
		cars - number of cars in simulation
		slowdown - fraction to cut speed by
		deer_timer - how often deer should jump out
		deer_count - how many deer should jump out (0 for random number)
		print_highway - do we want to see a simple animation
		"""
		self.tot_cars=cars
		self.slowdown=slowdown
		self.dt=dt
		self.cars=[]
		self.num_cars=0
		self.initial=True
		self.curtime=0.0
		self.waiting=False
		self.start_wait=0.0
		if print_highway:
			f, axarr = plt.subplots(2, sharex=True)
			axarr[0].set_xlim([0,self.length])
			axarr[0].set_ylim([-.1,.1])
			axarr[0].get_yaxis().set_visible(False)
			axarr[1].set_ylabel('Velocity (miles/second)')
			axarr[1].set_xlabel('Position (miles)')
			axarr[1].set_xlim([0,self.length])
			axarr[1].set_ylim([0,1.1])
			plt.ion()
			plt.show()
		while True:
			if self.approx(self.curtime%deer_timer,0) or self.approx(self.curtime%deer_timer,deer_timer):
				self.set_deer(deer_count,deer_prob)
				self.slow_for_deer(N)
			if self.num_cars<self.tot_cars:
				if self.approx(self.curtime%1,0) or self.approx(self.curtime%1,1):
					if self.initial:
						self.cars.append(car(None))
						self.num_cars += 1
						self.initial = False
					else:
						if not self.approx(self.cars[-1].get_vel(),1,0.02):
							if not self.waiting:
								self.waiting = True
								self.start_wait = self.curtime
						if (self.waiting and self.curtime-self.start_wait >= 5.0) or not self.waiting:
							self.waiting = False
							self.start_wait = 0.0
							self.cars.append(car(self.cars[-1]))
							self.num_cars += 1

			num_del = 0
			for i in range(len(self.cars)):
				if self.cars[i-num_del].get_pos()>self.length:
					del self.cars[i-num_del]
					num_del += 1
				else:
					self.cars[i-num_del].update(self.dt)
			if print_highway:
				positions = self.get_positions()
				velocities = self.get_velocities()
				yvals = [0]*len(positions)
				deer = self.get_deer()
				ydvals = [0]*len(deer)
				axarr[0].cla()
				axarr[0].set_xlim([0,self.length])
				axarr[0].set_ylim([-.1,.1])
				axarr[1].cla()
				axarr[0].get_yaxis().set_visible(False)
				axarr[1].set_ylabel('Velocity (miles/second)')
				axarr[1].set_xlabel('Position (miles)')
				axarr[1].set_xlim([0,self.length])
				axarr[1].set_ylim([0,1.1])
				axarr[0].plot(positions,yvals,'bo',deer,ydvals,'ro')
				axarr[1].bar(positions,velocities,0.2)
				plt.draw()
				#time.sleep(0.1)
			self.curtime += self.dt
			if len(self.cars) <= 0:
				break
		return self.curtime

	def approx(self,val,check=0,tolerance = 1e-5):
		"""
		since we are using floating points, the numbers are never exactly what
		we want. This checks if the number is approximately what we want (to
		within a tolerance)
		
		returns true if it is within tolerance, false if not.
		"""
		if np.fabs(val-check)<1e-5:
			return True
		return False

def f(x):
	road = highway(100)
	avg = []
	for i in range(25):
		avg.append([])
		for j in range(10):
			avg[i].append(road.run(0.1,1000,0.5,x,1,0,i/100.))
		print "{}% chance of deer: {} seconds ({} slowdown)".format(i,sum(avg[i])/len(avg[i]),x)
	return [sum(val)/len(val) for val in avg]

if __name__=="__main__":
	#road = highway(100)
	#print road.run(0.1,1000,0.5,10,1,0,0.04,True)
	#p = Pool(8)
	#print p.map(f,range(0,10))
	print f(10)
	
